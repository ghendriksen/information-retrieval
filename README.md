# Installation

* Install poetry
* Run `poetry install` to install the virtual environment
* Run `poetry shell` to enter the virtual environment

## MonetDB

For MonetDB, you have to setup the actual database. You can do this as follows:

* Run the MonetDB docker container:  
  ```
  $ docker run -d -p 0.0.0.0:50000:50000 --name monetdb --volume /path/to/project/.monetdb:/app monetdb/monetdb
  ```
* Connect to the container using:
  ```
  $ docker exec -it monetdb /bin/bash
  ```
* In the docker shell, run the following commands:
  ```
  # monetdb create oldduck
  # monetdb release oldduck
  # monetdb start oldduck
  ```
