def bm25(terms, disjunctive=True):
    term_list = ', '.join([f"'{term}'" for term in terms])

    constraint = '' if disjunctive else 'HAVING COUNT(distinct termid) = (SELECT COUNT(*) FROM termids)'

    return f"""
        WITH termids AS (SELECT termid FROM dict WHERE term IN ({term_list})),
            qterms AS (SELECT termid, docid, count FROM terms 
            WHERE termid IN (SELECT * FROM termids)), 
            subscores AS (SELECT docs.docid, length, term_tf.termid, 
            tf, df, (ln(((SELECT COUNT(*) FROM docs WHERE length > 0)-df+0.5)/(df+0.5))*((tf*(1.2+1)/
            (tf+1.2*(1-0.75+0.75*(length/(SELECT AVG(length) FROM docs WHERE length > 0))))))) AS subscore 
            FROM (SELECT termid, docid, count AS tf FROM qterms) AS term_tf 
            JOIN (SELECT docid FROM qterms 
                GROUP BY docid {constraint})
                AS cdocs ON term_tf.docid = cdocs.docid 
            JOIN docs ON term_tf.docid=docs.docid 
            JOIN dict ON term_tf.termid=dict.termid) 
        SELECT docs.name, score FROM (SELECT docid, sum(subscore) AS score 
            FROM subscores GROUP BY docid) AS scores JOIN docs ON 
            scores.docid=docs.docid ORDER BY score DESC;
    """
