import duckdb
import json
import os
import math
import pymonetdb
import numpy as np
import pandas as pd
from abc import ABC, abstractmethod
from collections import defaultdict
from nltk import corpus, word_tokenize
from pymonetdb.sql import cursors as monet_cursors
from typing import Union, Dict
import time


class Index(ABC):
    cursor: Union[duckdb.Cursor, monet_cursors.Cursor]

    def __init__(self, db):
        self.stopwords = corpus.stopwords.words('english')
        self.init_db(db)

    def get_terms(self, body: str) -> Dict[str, int]:
        """
        Preprocesses a document's body by tokenising it and removing stop words and other words that contain
        characters other than letters

        :param body: the contents of the document
        :return: a mapping of terms in the document to their occurrence frequency
        """
        terms = defaultdict(int)

        for term in word_tokenize(body.lower()):
            if term not in self.stopwords and term.isalpha():
                terms[term] += 1

        return terms

    @staticmethod
    def get_index(engine: str, db: str) -> 'Index':
        """
        Returns an index able to run on the specified database engine

        :param engine: the desired database engine (duckdb or monetdb)
        :param db: the name of the database that should be used
        :return: an index capable of running on the specified database engine
        """
        if engine == 'duckdb':
            return DuckDBIndex(db)
        elif engine == 'monetdb':
            return MonetDBIndex(db)
        raise NotImplementedError(f'Engine "{engine}" not implemented!')

    @abstractmethod
    def init_db(self, db: str) -> None:
        """
        Runs the initial startup of the database, for instance by creating the necessary tables.

        Should initialise `self.cursor` correctly.

        :param db: the name of the database that should be used
        """
        pass

    @abstractmethod
    def index(self, document: dict) -> None:
        """
        Indexes a document

        :param document: a dictionary with the `name` and `body` of the document
        """
        pass

    @abstractmethod
    def get_existing_tables(self) -> Dict[str, pd.DataFrame]:
        """
        Retrieves the full contents of the existing tables. Used for the bulk index, where all
        heavy computation is done in Python rather than by the database using SQL, for faster
        execution.

        :return: a dictionary containing the `dict`, `terms` and `docs` tables as pandas DataFrames
        """
        pass

    @abstractmethod
    def export_tables_to_database(self, tables: Dict[str, pd.DataFrame]) -> None:
        """
        Exports the generated DataFrames to the database.

        :param tables: a dictionary containing the `dict`, `terms` and `docs` tables as pandas DataFrames
        """
        pass

    def update_dict_table(self, tables: Dict[str, pd.DataFrame], doc_terms: Dict[str, int]):
        """
        Updates the `dict` table in DataFrame form. Adds previously unseen terms into the
        `dict` table with a unique id, and increases the document frequency of the other terms by one.

        :param tables: a dictionary containing the `dict`, `terms` and `docs` tables as pandas DataFrames
        :param doc_terms: a mapping of terms in the document to their occurrence frequency
        """

        # Generate a new dict row for each term, to make sure the term exists in the dict table
        new_dict = pd.DataFrame([{
            'term': term,
            'df': 0,
        } for term in doc_terms])

        # Add the new rows to the existing dict table, generating new unique ids for each term
        # Also, drop all added rows with terms that already existed
        dict_table = (pd.concat([tables['dict'], new_dict], ignore_index=True, sort=False)
                      .drop_duplicates('term'))
        # For each of the terms in the document, increase its document frequency
        dict_table.loc[dict_table['term'].isin(doc_terms), 'df'] += 1

        tables['dict'] = dict_table

    def update_term_table(self, tables: Dict[str, pd.DataFrame], doc_terms: Dict[str, int], docid: int):
        """
        Updates the `term` table in DataFrame form. For each term in the document, it adds a new row to
        the `term` table containing the term's id, the id of the current document and the amount of times
        the term occurs in the current document.

        :param tables: a dictionary containing the `dict`, `terms` and `docs` tables as pandas DataFrames
        :param doc_terms: a mapping of terms in the document to their occurrence frequency
        :param docid: the id of the document under consideration
        """

        # Copy the rows from the dict table to create the new entries for the terms table
        new_terms = tables['dict'].loc[tables['dict']['term'].isin(doc_terms)].copy()

        # Create columns containing the correct term and document ids
        # The term ids can be extracted directly from the index of the copied rows, and the
        # document ids are the same for each row
        new_terms['termid'] = new_terms.index
        new_terms['docid'] = np.repeat(docid, len(doc_terms))

        # Replace the `term` column by a `count` column that contains the amount of times
        # the term occurs in the document, and make sure only the `termid`, `docid` and `count`
        # columns remain
        new_terms = (new_terms.replace({'term': doc_terms})
                              .rename(columns={'term': 'count'})[['termid', 'docid', 'count']])

        # Add the new term entries
        tables['terms'] = pd.concat([tables['terms'], new_terms], ignore_index=True)

    def bulk_index(self, filename: str) -> None:
        """
        Reads a JSON file for bulk indexing purposes. Each item in the JSON array should be a dictionary containing
        the document `name` and `body`. Instead of using the `index` function repeatedly, this procedure does the
        heavy lifting using pandas DataFrames. This speeds up index time significantly, mostly due to the lack of
        execution of SQL queries.

        :param filename: the name of the file in which all documents can be found
        """

        try:
            with open(filename) as _file:
                data = json.load(_file)
        except json.JSONDecodeError:
            print('[!] Invalid input file!')
            return

        print('Indexing...')

        tables = self.get_existing_tables()

        # Set the id as dataframe index to make use of the automatic
        # unique id generation in dataframes
        if not tables['dict'].empty:
            tables['dict'].set_index('termid', inplace=True)

        docs = []

        # Generate unique ids by starting above the largest one and counting up
        docid_start = 1 if tables['docs'].empty else tables['docs']['docid'].max() + 1

        start = time.time()

        for i, document in enumerate(data):
            docid = docid_start + i
            doc_terms = self.get_terms(document['body'])

            self.update_dict_table(tables, doc_terms)
            self.update_term_table(tables, doc_terms, docid)

            docs.append({
                'docid': docid,
                'name': document['name'],
                'length': sum(doc_terms.values()),
            })

            amount_of_digits = math.floor(math.log10(len(data))) + 1
            print(f'{i + 1:>{amount_of_digits}d}/{len(data)}', end='\r')

        new_docs = pd.DataFrame(docs, columns=['docid', 'name', 'length'])
        tables['docs'] = new_docs if tables['docs'].empty else pd.concat([tables['docs'], new_docs], ignore_index=True)
        tables['dict']['termid'] = tables['dict'].index
        tables['dict'] = tables['dict'][['termid', 'term', 'df']]

        self.export_tables_to_database(tables)

        current = time.time()
        print(f'Indexed {len(data)} documents in {current - start:.2f} seconds!')

    @abstractmethod
    def search(self, query) -> list:
        """
        Performs an SQL query on the index. Is used in combination with the `search` and `query` modules, which
        contain predefined queries like BM25.

        :param query: an SQL query to be performed on the index
        :return: a list of query results
        """
        pass

    @abstractmethod
    def clear(self) -> None:
        """
        Deletes all entries from the index.
        """
        pass


class DuckDBIndex(Index):
    def reset_auto_increment(self):
        max_termid = self.cursor.execute('SELECT MAX(termid) FROM terms').fetchone()[0]
        max_termid = 1 if max_termid is None else max_termid + 1

        max_docid = self.cursor.execute('SELECT MAX(docid) FROM docs').fetchone()[0]
        max_docid = 1 if max_docid is None else max_docid + 1

        self.cursor.execute('DROP SEQUENCE term_ids')
        self.cursor.execute('DROP SEQUENCE doc_ids')
        self.cursor.execute(f'CREATE SEQUENCE term_ids START WITH {max_termid}')
        self.cursor.execute(f'CREATE SEQUENCE doc_ids START WITH {max_docid}')

    def init_db(self, db):
        db_exists = os.path.exists(db)

        self.cursor = duckdb.connect(db).cursor()

        if not db_exists:
            self.cursor.execute(f'CREATE SEQUENCE term_ids')
            self.cursor.execute(f'CREATE SEQUENCE doc_ids')
            self.cursor.execute('CREATE TABLE dict('
                                'termid INTEGER NOT NULL,'
                                'term VARCHAR NOT NULL,'
                                'df INTEGER NOT NULL)')
            self.cursor.execute('CREATE TABLE docs('
                                'docid INTEGER NOT NULL,'
                                'name VARCHAR NOT NULL,'
                                'length INTEGER NOT NULL)')
            self.cursor.execute('CREATE TABLE terms('
                                'termid INTEGER NOT NULL,'
                                'docid INTEGER NOT NULL,'
                                'count INTEGER NOT NULL)')
        else:
            self.reset_auto_increment()

    def index(self, document: dict):
        terms = self.get_terms(document['body'])
        doc_name = document['name']
        doc_length = sum(terms.values())
        doc_id = self.cursor.execute("SELECT nextval('doc_ids')").fetchone()[0]

        # Insert document into `docs` table
        self.cursor.execute(f"INSERT INTO docs VALUES ({doc_id}, '{doc_name}', {doc_length})")

        term_freqs = {}

        for term, frequency in terms.items():
            # Check if term is already present in the `dict` table
            term_id = self.cursor.execute(f"SELECT termid FROM dict WHERE term = '{term}'").fetchone()

            if term_id is None:
                # If the term is not yet present, generate a new id and create an entry for the term
                term_id = self.cursor.execute("SELECT nextval('term_ids')").fetchone()[0]
                self.cursor.execute(f"INSERT INTO dict VALUES ({term_id}, '{term}', 1)")
            else:
                # If the term is already present, increase its document frequency by one
                term_id = term_id[0]
                self.cursor.execute(f"UPDATE dict SET df = df + 1 WHERE termid = {term_id}")

            term_freqs[term_id] = frequency

        # Insert all combinations of term id and doc id into the `terms` table
        term_values = ', '.join([f'{term_id, doc_id, frequency}' for term_id, frequency in term_freqs.items()])
        self.cursor.execute(f"INSERT INTO terms VALUES {term_values}")

    def get_existing_tables(self) -> Dict[str, pd.DataFrame]:
        dict_table = self.cursor.execute('SELECT * FROM dict').fetchdf()
        term_table = self.cursor.execute('SELECT * FROM terms').fetchdf()
        doc_table = self.cursor.execute('SELECT * FROM docs').fetchdf()

        return {
            'dict': dict_table,
            'terms': term_table,
            'docs': doc_table,
        }

    def export_tables_to_database(self, tables: Dict[str, pd.DataFrame]) -> None:
        for table in ('dict', 'docs', 'terms'):
            tables[table].to_csv(f'{table}.csv', header=False, index=False)

            self.cursor.execute(f'DELETE FROM {table}')
            self.cursor.execute(f"COPY {table} FROM '{table}.csv'")
            os.remove(f'{table}.csv')

    def search(self, query):
        self.cursor.execute(query)
        df = self.cursor.fetchdf()
        return list(df.itertuples(index=False, name=None))[:10]

    def clear(self):
        self.cursor.execute("DELETE FROM terms")
        self.cursor.execute("DELETE FROM docs")
        self.cursor.execute("DELETE FROM dict")

    def __str__(self):
        dict_rows = self.cursor.execute('SELECT * FROM dict').fetchdf()
        term_rows = self.cursor.execute('SELECT * FROM terms').fetchdf()
        doc_rows = self.cursor.execute('SELECT * FROM docs').fetchdf()

        return '\n'.join([
            'dict',
            '-' * 20,
            str(dict_rows),
            '',
            'terms',
            '-' * 20,
            str(term_rows),
            '',
            'docs',
            '-' * 20,
            str(doc_rows),
        ])


class MonetDBIndex(Index):
    def init_db(self, db: str):
        self.cursor = pymonetdb.connect(username='monetdb', password='monetdb',
                                        hostname='localhost', database=db).cursor()

        self.cursor.execute('CREATE TABLE IF NOT EXISTS dict('
                            # 'termid INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,'
                            'termid INTEGER NOT NULL,'
                            # 'term VARCHAR(32) NOT NULL UNIQUE,'
                            'term VARCHAR(64) NOT NULL,'
                            'df INTEGER NOT NULL)')
        self.cursor.execute('CREATE TABLE IF NOT EXISTS docs('
                            # 'docid INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,'
                            'docid INTEGER NOT NULL,'
                            'name VARCHAR(64) NOT NULL,'
                            'length INTEGER NOT NULL)')
        self.cursor.execute('CREATE TABLE IF NOT EXISTS terms('
                            'termid INTEGER NOT NULL,'
                            'docid INTEGER NOT NULL,'
                            'count INTEGER NOT NULL)')

    def index(self, document: dict):
        terms = self.get_terms(document['body'])
        doc_name = document['name'][:32]
        doc_length = sum(terms.values())

        self.cursor.execute(f"INSERT INTO docs (name, length) VALUES ('{doc_name}', {doc_length})")
        doc_id = self.cursor.lastrowid

        for term, frequency in terms.items():
            rows = self.cursor.execute(f"SELECT termid FROM dict WHERE term = '{term}'")
            if rows > 0:
                term_id, = self.cursor.fetchone()
                self.cursor.execute(f"UPDATE dict SET df = df + 1 WHERE termid = {term_id}")
            else:
                self.cursor.execute(f"INSERT INTO dict (term, df) VALUES ('{term}', 1)")
                term_id = self.cursor.lastrowid

            self.cursor.execute(f"INSERT INTO terms VALUES ({term_id}, {doc_id}, {frequency})")
        self.cursor.execute('COMMIT')

    def get_existing_tables(self) -> Dict[str, pd.DataFrame]:
        self.cursor.execute('SELECT * FROM dict')
        dict_table = pd.DataFrame(self.cursor.fetchall(), columns=['termid', 'term', 'df'])

        self.cursor.execute('SELECT * FROM terms')
        term_table = pd.DataFrame(self.cursor.fetchall(), columns=['termid', 'docid', 'count'])

        self.cursor.execute('SELECT * FROM docs')
        doc_table = pd.DataFrame(self.cursor.fetchall(), columns=['docid', 'name', 'length'])

        return {
            'dict': dict_table,
            'terms': term_table,
            'docs': doc_table,
        }

    def export_tables_to_database(self, tables: Dict[str, pd.DataFrame]) -> None:
        for table in ('dict', 'docs', 'terms'):
            tables[table].to_csv(f'.monetdb/{table}.csv', header=False, index=False)

            self.cursor.execute(f'DELETE FROM {table}')
            self.cursor.execute(f"COPY INTO {table} FROM '/app/{table}.csv' USING DELIMITERS ','")

            os.remove(f'.monetdb/{table}.csv')

        self.cursor.execute('COMMIT')

    def search(self, query):
        self.cursor.execute(query)
        return self.cursor.fetchmany(10)

    def clear(self):
        self.cursor.execute('TRUNCATE terms RESTART IDENTITY')
        self.cursor.execute('TRUNCATE docs RESTART IDENTITY')
        self.cursor.execute('TRUNCATE dict RESTART IDENTITY')
        self.cursor.execute('COMMIT')

    def __str__(self):
        table_mapping = {
            'dict': ['termid', 'term', 'df'],
            'docs': ['docid', 'name', 'length'],
            'terms': ['termid', 'docid', 'count'],
        }

        rows = []

        for table, col_names in table_mapping.items():
            rows.append([table])
            rows.append(['-' * (11 * len(col_names) - 1)])
            rows.append(col_names)
            rows.append(['-' * 10 for _ in range(len(col_names))])

            amount = self.cursor.execute(f'SELECT * FROM {table}')
            rows.extend(self.cursor.fetchmany(10))

            if amount > 10:
                rows.append(['...' for _ in range(len(col_names))])
            rows.append('')

        return '\n'.join([' '.join([f'{value:>10}' for value in row]) for row in rows])
