from index import Index
import query


class Search:
    index: Index

    def __init__(self, index: Index):
        self.index = index

    def search(self, terms, method='bm25'):
        terms = list(self.index.get_terms(' '.join(terms)).keys())

        if method == 'bm25':
            sql_query = query.bm25(terms, False)
        else:
            raise NotImplementedError(f'Search method "{method}" was not implemented')

        return self.index.search(sql_query)
